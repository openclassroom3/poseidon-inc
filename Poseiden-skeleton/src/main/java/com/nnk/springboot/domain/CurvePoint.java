package com.nnk.springboot.domain;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Builder
@Table(name = "curvepoint")
public class CurvePoint implements com.nnk.springboot.api.domain.ICurvePoint {
// TODO: Map columns in data table CURVEPOINT with corresponding java fields

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Integer curveId;

    Timestamp asOfDate;

    @NonNull
    Double term;

    @NonNull
    Double value;

    Timestamp creationDate;

}
