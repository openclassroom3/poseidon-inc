package com.nnk.springboot.domain;

import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Builder
@Table(name = "rulename")
public class RuleName {
// TODO: Map columns in data table RULENAME with corresponding java fields

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Integer id;;

    String name;
    String description;
    String json;
    String template;
    String sqlStr;
    String sqlPart;

}
